#!/usr/bin/env ruby
require 'json'
require 'time'
require 'faraday'
require 'faraday/follow_redirects'
require 'optimist'
require 'ostruct'
require 'tty-table'
require 'pry-rescue'
require 'linear_regression_trend'
require 'awesome_print'


module GemEntry
  def catalogEntry
    "#{name}: #{info}"
  end

  def totalVersions
    versions.size
  end

  def latestVersionDate
    sortedVersions.first.publishedDate
  end

  def avgDateOfAllVersions
    sum = versions.inject(0) { |memo,version|
      memo += version.publishedDate.to_time.to_i
    }
    Time.at(sum / totalVersions)
  end

  def sortedVersions
    versions.sort {|a,b| a.publishDate <=> b.publishDate}
  end
  def versionDownloadHistory
    res = Array.new
    nextRelease = Date.today
    sortedVersions.each {|version|
      version[:downloadsPerDay] = version.downloadsPerDayFrom(nextRelease)
      res.push(version.to_h)
      nextRelease = version.publishedDate
    }
    res.collect{|each|each[:downloadsPerDay]}
  end

  def downloadSlope
    slope = LinearRegressionTrend::Calculator.new(versionDownloadHistory).slope
    slope.nan? ? 0 : slope
  end


  def uriScore
    if @uriScore.nil?
      @uriScore = 0.0
      uriCount = 0
      foundCount = 0
      [:homepage_uri, :source_code_uri].each {|uri|
        if not self[uri].nil? and (self[uri].size > 0)
          print "Attempting #{self[uri]}"
          uriCount += 1
          begin
            client = Faraday.new(url: self[uri]) do |faraday|
              faraday.response(:follow_redirects)
              faraday.options.timeout = 5
              faraday.adapter(Faraday.default_adapter)
            end
            res = client.get
            foundCount += 1 if (res.status == 200)
            puts
          rescue Exception => e
            ap e
          end
        end
      }
      @uriScore = uriCount == 0.0 ? 0 : foundCount.to_f / uriCount.to_f
    end
    @uriScore
  end

end

module GemVersion
  def publishedDate
    Date.iso8601(built_at)
  end
  def downloadsPerDayFrom(aDate)
    begin
      downloads_count / (aDate - publishedDate).to_i
    rescue ZeroDivisionError
      downloads_count
    end
  end
end


##################################################################################################################
#

opts = Optimist::options do
  banner <<-EOS
Usage:
  hiddengems.rb [options] '<search term>'

Where [options] are:
EOS
  opt :links, "Verify gem's URIs before scoring (slow)", type: :boolean, default: false
  opt :downloads, "Weight of download count", type: :integer, default: 1
  opt :versions, "Weight of version count", type: :integer, default: 1
  opt :latest_version_date, "Weight of latest version date", type: :integer, default: 1
  opt :avg_version_date, "Weight of average version date", type: :integer, default: 1
  opt :download_slope, "Weight of download slope", type: :integer, default: 1
  opt :rejectfilter, "Regular Expression of gem names to reject", type: :string
end

puts "Option values:"
ap opts

searchTerm = ARGV[0]
puts "\nSearch term: #{searchTerm}\n"


curpage = 1
gems = Array.new
gemdict = Hash.new
until curpage == 0
  dupepad = 0
  gemPage = JSON.parse(Faraday.get("https://rubygems.org/api/v1/search.json?page=#{curpage}&query=#{searchTerm}").body).collect { |each| OpenStruct.new(each)}.collect {|gem|
    gem.extend(GemEntry)
    if gemdict.has_key?(gem.name)
      puts "(rejected a dupe)"
      dupepad += 1
      next
    end
    gem[:versions] = JSON.parse(
      Faraday.get(
          "https://rubygems.org/api/v1/versions/#{gem.name}.json"
      ).body
    ).collect { |version|
      version = OpenStruct.new(version).extend(GemVersion)
    }
    gem[:rank] = 0
    gemdict[gem.name] = gem
    gem
  }.reject(&:nil?)
  gems += gemPage
  puts "Found +#{gemPage.size} gems (total #{gems.size})"
  curpage = (((gemPage.size + dupepad) < 30) ? 0 : curpage.next)
  puts "looking for more..." if curpage != 0
end

puts "\nSearch term '#{searchTerm}' returned #{gems.size} hits"
if opts[:rejectfilter_given]
  puts "Filtering out unwanted gems with #{opts[:rejectfilter]}"
  re = Regexp.compile(opts[:rejectfilter])
  gems.reject! {|each|
    re.match(each.name)
  }
  puts "\n#{gems.size} remaining gems after reject filter"
end


if opts[:links]
  puts "\nValidating URIs..."
  gems.reject! {|each|
    if each.uriScore == 0.0
      puts "Rejecting #{each.name} due to lack of valid URIs"
      true
    else
      false
    end
  }
  puts "\n#{gems.size} remaining gems after link validation"
end

puts "\nCalculating rankings..."

data = Hash.new
data[:downloads] = gems.sort {|a,b| b.downloads <=> a.downloads}.collect {|each| "#{each.name} (#{each.downloads})"}
data[:versions] = gems.sort {|a,b| b.totalVersions <=> a.totalVersions}.collect {|each| "#{each.name} (#{each.totalVersions})"}
data[:latest_version_date] = gems.sort {|a,b| b.latestVersionDate <=> a.latestVersionDate}.collect {|each| "#{each.name} (#{each.latestVersionDate.year})"}
data[:avg_version_date] = gems.sort {|a,b| b.avgDateOfAllVersions<=> a.avgDateOfAllVersions}.collect {|each| "#{each.name} (#{each.avgDateOfAllVersions.year})"}
data[:download_slope] = gems.sort {|a,b| b.downloadSlope <=> a.downloadSlope}.collect {|each| "#{each.name} (#{each.downloadSlope.round})"}
report = Array.new
rank = 0
gems.each_index {|index|
  record = Hash.new
  record[:rank] = rank
  data.keys.each {|column|
    record[column] = data[column][index]
    gemdict[data[column][index].split(' (').first][:rank] += index * opts[column]
  }
  report.push(record)
  rank += 1
}
puts
puts "Individual Rankings (best -> worst)"
puts  TTY::Table.new(header: [:rank]+data.keys.collect {|each| "#{each} (#{opts[each]})" }, rows: report).render(:ascii, resize: false, column_widths: [5] + [30] * data.keys.size)

puts "\nRank sums (best -> worst)"
puts TTY::Table.new(header: [:score, :name,:info], rows: gems.sort {|a,b| a.rank<=>b.rank}.collect {|each| [each.rank,each.name,each.info]}).render(:ascii,  column_widths: [5,35,118],resize: false)

puts
