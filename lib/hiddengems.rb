# frozen_string_literal: true

require_relative "hiddengems/version"

module Hiddengems
  class Error < StandardError; end
  # Your code goes here...
end
