# Hiddengems

This gem provides an executable - hiddengems.rb - that submits a search term to rubygems.org 
and sorts the search results by a number of ranking parameters:

- number of downloads
- number of versions
- date of latest version
- average date of all released versions
- download linear regression trend slope

Using the above, it produces two tables:

The first is comprised of 5 lists of individual rankings sorted best -> worst for each metric. 

The second table contains a list of gems and their descriptions sorted by the *sum* of all a given gems ranks from the first table.  The lowest overall score is best.   By default each individual ranking is given a weight of 1, but you can alter this with command line options (see below)

![illustration of how scoring works by default](img/2022-09-04_13-40.png "Scoring example")

The rationale for this is that ranking based on download count alone (e.g. http://bestgems.org) doesn't always give you a good sense of where things stand for a given search term.  Looking at download count alone, a gem that has been around for many years and has garnered many downloads but has stagnated will tend to retain its top ranking due to the fact that people will tend to download the top ranking gem first.  In other words: it invites a positive feedback situation.

hiddengems.rb attempts to do a better job of ranking for a given search term.  As it's grabbing all the requisite data at run time, it's not as fast as using an indexing site, but it's definitely "smarter".

The program supports options that allow you to adjust the weights of the metrics that are used to calculate the rank sum table.  By default all metrics are given equal weight, but you can tweak this as you see fit.

Example:
```
hiddengems.rb 'zabbix sender' --links --downloads 0 --avg-version-date 3 --download-slope 3 --latest-version-date 2
```
gives:
![illustration of how scoring works with weight adjustments](img/2022-09-04_13-51.png "Weighted Scoring example")

The program also includes a link validation function.  If you use it (--links), hiddengems.rb will first audit each gem's homepage_uri and source_code_uri links to ensure that (a) at least one of those URIs is populated and (b) that the links that are defined are valid.  If both (a) and (b) fail, the gem is removed from the list of found gems before ranking is calculated.

The --links option can add a bunch of time to processing so it's not on by default, but it does a good job of weeding out dead projects.

Note: If you end up submitting a search term that's pretty broad, you might end up with a large -> huge -> gigantic result set.  While you can let it run to completion, you're probably better off adding additional words to your search - rubygems interprets search phrases as "word1 AND word2 AND word3..." so the more specific you get the less results you'll receive.

Also note: you'll almost certainly want to pipe output to a pager (e.g. "| less") as the output tends to be pretty long line-wise.

Example:

```
$ ./hiddengems.rb --help
Usage:
  hiddengems.rb [options] '<search term>'

Where [options] are:
  -l, --links                      Verify gem's URIs before scoring (slow)
  -d, --downloads=<i>              Weight of download count (default: 1)
  -v, --versions=<i>               Weight of version count (default: 1)
  -a, --latest-version-date=<i>    Weight of latest version date (default: 1)
  -g, --avg-version-date=<i>       Weight of average version date (default: 1)
  -o, --download-slope=<i>         Weight of download slope (default: 1)
  -r, --rejectfilter=<s>           Regular Expression of gem names to reject (default: )
  -h, --help                       Show this message


$ hiddengems.rb 'zabbix sender' --links  
Option values:
{
                  :links => true,
              :downloads => 1,
               :versions => 1,
    :latest_version_date => 1,
       :avg_version_date => 1,
         :download_slope => 1,
                   :help => false,
            :links_given => true
}

Search term: zabbix sender
Found +6 gems (total 6)

Search term 'zabbix sender' returned 6 hits

Validating URIs...
Attempting https://github.com/nownabe/zabbix_sender
Attempting https://svdasein.gitlab.io/zabbix_sender_api
Attempting https://gitlab.com/svdasein/zabbix_sender_api
Attempting https://github.com/lehn-etracker/zabbix_sender_legacy
Attempting https://github.com/eduvo/zabbix-ruby-client
Attempting https://github.com/englishm/zabbix_send
Attempting https://github.com/englishm/zabbix_send
Rejecting zabbix_receiver due to lack of valid URIs

5 remaining gems after link validation

Calculating rankings...

Individual Rankings (best -> worst)
+-----+------------------------------+------------------------------+------------------------------+------------------------------+------------------------------+
|rank |downloads (1)                 |versions (1)                  |latest_version_date (1)       |avg_version_date (1)          |download_slope (1)            |
+-----+------------------------------+------------------------------+------------------------------+------------------------------+------------------------------+
|0    |zabbix-ruby-client (51536)    |zabbix-ruby-client (24)       |zabbix_sender_api (2022)      |zabbix_sender_api (2021)      |zabbix_send (1289)            |
|1    |zabbix_sender (35317)         |zabbix_sender_api (11)        |zabbix_sender (2020)          |zabbix_sender (2017)          |zabbix_sender (320)           |
|2    |zabbix_send (13477)           |zabbix_sender (5)             |zabbix_sender_legacy (2016)   |zabbix_sender_legacy (2016)   |zabbix_sender_legacy (279)    |
|3    |zabbix_sender_api (10149)     |zabbix_sender_legacy (5)      |zabbix-ruby-client (2016)     |zabbix-ruby-client (2014)     |zabbix_sender_api (100)       |
|4    |zabbix_sender_legacy (9989)   |zabbix_send (3)               |zabbix_send (2013)            |zabbix_send (2013)            |zabbix-ruby-client (64)       |
+-----+------------------------------+------------------------------+------------------------------+------------------------------+------------------------------+

Rank sums (best -> worst)
+-----+-----------------------------------+----------------------------------------------------------------------------------------------------+
|score|name                               |info                                                                                                |
+-----+-----------------------------------+----------------------------------------------------------------------------------------------------+
|6    |zabbix_sender                      |Zabbix Sender gem                                                                                   |
|7    |zabbix_sender_api                  |This gem implements an api that abstracts the zabbix sender/trapper mechanism. It saves tons of ti… |
|10   |zabbix-ruby-client                 |A zabbix alternative to zabbix-agent using zabbix-sender.                                           |
|13   |zabbix_sender_legacy               |Improved zabbix_sender gem just for legacy ruby &lt; 2.0 like in Debian Wheezy                      |
|14   |zabbix_send                        |Send data to Zabbix trappers from Ruby                                                              |
+-----+-----------------------------------+----------------------------------------------------------------------------------------------------+


```

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'hiddengems'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install hiddengems

## Usage

    $ hiddengems.rb 'search term'

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/svdasein/hiddengems.
