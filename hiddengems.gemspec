# frozen_string_literal: true

require_relative "lib/hiddengems/version"

Gem::Specification.new do |spec|
  spec.name          = "hiddengems"
  spec.version       = Hiddengems::VERSION
  spec.authors       = ["Dave Parker"]
  spec.email         = ["dparker@svdasein.org"]

  spec.summary       = "Search for ruby gems with a search term (like bestgems.org) with better ranking"
  spec.description   = "Provides a cli that lets you search rubygems for a search term.  Displays multiple ranking criteria with emphasis on project vitality. Alternative to rubygems.org"
  spec.homepage      = "https://gitlab.com/svdasein/hiddengems"
  spec.required_ruby_version = ">= 2.4.0"
  spec.license = 'MIT'

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/svdasein/hiddengems"
  spec.metadata["changelog_uri"] = "https://gitlab.com/svdasein/hiddengems/CHANGELOG"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  # spec.add_dependency "example-gem", "~> 1.0"
  spec.add_dependency 'json'
  spec.add_dependency 'time'
  spec.add_dependency 'faraday','~>2.5.2'
  spec.add_dependency 'faraday-follow_redirects','~>0.3.0'
  spec.add_dependency 'optimist'
  spec.add_dependency 'ostruct'
  spec.add_dependency 'tty-table','~>0.12.0'
  spec.add_dependency 'awesome_print','~>1.9.2'
  spec.add_dependency 'pry-rescue'
  spec.add_dependency 'linear_regression_trend','~>1.2.0'

  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
end
